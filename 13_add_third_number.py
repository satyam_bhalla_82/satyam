## Adding third number from one list to another
print("Enter elements in a list")

##  Taking input in list
l1 = list(map(int,input().split()))

print("Creating another list with specified output")
##Setting initial variables
l2 = []
count = 2
i = 2

## Empty the list
while len(l1) != 0:
    l2.append(l1[i])
    l1.remove(l1[i])
    if len(l1)==0:
        break
    else:
        i = (i+count)%len(l1)
print(l2)
