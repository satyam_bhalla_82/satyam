# Function to count frequency of each element 
import collections
import operator
 
# it returns a dictionary data structure whose 
# keys are array elements and values are their 
# corresponding frequencies {1: 4, 2: 4, 3: 2, 
# 5: 2, 4: 1}
def CountFrequency(arr):
    return collections.Counter(arr)
## Approach used calculate the frequency of each item and make a python dictionary
## After that sort that dictionary based upon the dictionary values in descending order
## After that skip the first key and start incrementing other dictionary values
## Print the maximum count of common elements
 
# Driver function
if __name__ == "__main__":
    print("Enter the elements")
    arr = list(map(int,input().split()))
    print("Enter the value of q:")
    q = int(input())
    freq = CountFrequency(arr)
    # iterate dictionary named as freq to print
    freq_descen = collections.OrderedDict(sorted(freq.items(), key=lambda x: x[1],reverse=True))
    # count of each element
    l2 = list(freq_descen.keys())
    for key, value in freq_descen.items():
        print(key, " -> ", value)
        
    for key in freq_descen.keys():
        if key== l2[0]:
            pass
        else:
            while q != 0:
                if key == l2[0]:
                    pass
                else:
                    freq_descen[key] = freq_descen[key]+1
                    q = q-1
