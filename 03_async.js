//The following code is using set timeout function
const arr = [10, 12, 15, 21];
for (var i = 0; i < arr.length; i++) {
  setTimeout(function() {
    console.log('Index: ' + i + ', element: ' + arr[i]);
  }, 3000);
}
// Output of the above code is
Index: 4, element: undefined
Index: 4, element: undefined
Index: 4, element: undefined
Index: 4, element: undefined

/*To get the desired output we can use the let keyword in loop initialization
  var keyword is making the variable i global due to Hoisting so let variable
  can make the scope global */

//Here are the changes
const arr = [10, 12, 15, 21];
for (let i = 0; i < arr.length; i++) { //var is replaced with let
  setTimeout(function() {
    console.log('Index: ' + i + ', element: ' + arr[i]);
  }, 3000);
}
// Output of the above code is
Index: 0, element: 10
Index: 1, element: 12
Index: 2, element: 15
Index: 3, element: 21


